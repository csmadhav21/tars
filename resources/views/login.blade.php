@extends('layouts.basic')

@section('title','Login')

@section('content')
@include('layouts.logo')
<form action="{{route('login.login.post')}}" method="POST">
    <div class="uk-margin-top">
    <div class="uk-card uk-card-default uk-margin-auto uk-margin-auto-vertical" style="width: 400px;">
        <div class="uk-card-header">
            <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-expand">
                    <h3 class="uk-card-title uk-margin-remove-bottom">Login</h3>
                </div>
                <a href="{{route('register.index.get')}}">Register here</a>
            </div>
        </div>
        <div class="uk-card-body">
            @include('flash::message')
        
            @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                      <div class="uk-alert-danger" uk-alert>
                          <a class="uk-alert-close" uk-close></a>
                          <p>{{$error}}</p>
                      </div>
                    @endforeach
            @endif

            <div class="uk-margin">
                <label class="uk-form-label">Email:</label>
                <div class="uk-form-controls uk-margin-top-small">
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-form-width-large uk-input" type="email" name="email" value="{{old('email')}}" placeholder="Type your email here..." required>
                    </div>
                </div>
            </div>
            <div class="uk-margin">
                <label class="uk-form-label">Password : </label>
                <div class="uk-form-controls">
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: lock"></span>
                        <input class="uk-form-width-large uk-input" type="password" name="password" placeholder="Type your Secret" required>
                    </div>
                </div>
            </div>
            <div class="uk-position-bottom-left uk-margin-small-bottom uk-margin-medium-top uk-margin-left">
                <a href="{{route('forgotPassword.index.get')}}"><span class="uk-text-small">Forgot password?</span></a>
            </div>
            {{csrf_field()}}
            <center><input type="submit" class="uk-button uk-margin-auto uk-button-primary" value="Log in" /></center>
            
        </div>
    </div>
    </div>
</form>
@endsection
