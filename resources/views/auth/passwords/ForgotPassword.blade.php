@extends('layouts.basic')

@section('title','Forgot Password')

@section('content')
@include('layouts.logo')
<form action="{{route('forgotPassword.sendMailAfterAuth.post')}}" method="POST">
  <div class="uk-margin-small-top">
    <div class="uk-card uk-card-default uk-margin-auto uk-margin-auto-vertical" style="width: 400px;">
      <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
          <div class="uk-width-expand">
              <h3 class="uk-card-title uk-margin-remove-bottom">Forgot Password</h3>
          </div>
          <a href="{{route('login.index.get')}}">Login here</a>
        </div>
      </div>
      <div class="uk-card-body">
        @if (count($errors) > 0)
          @foreach ($errors->all() as $error)
            <div class="uk-alert-danger" uk-alert>
              <a class="uk-alert-close" uk-close></a>
              <p>{{$error}}</p>
            </div>
          @endforeach
        @endif
        @include('flash::message')
        <div class="uk-margin">
            <label class="uk-form-label">Email:</label>
            <div class="uk-form-controls uk-margin-top-small">
                <div class="uk-inline">
                  <span class="uk-form-icon" uk-icon="icon: user"></span>
                  <input class="uk-form-width-large uk-input" type="email" name="email" value="{{old('email')}}" placeholder="Type your email here..." required>
                </div>
            </div>
        </div>
        {{csrf_field()}}
        <center>
          <input type="submit" class="uk-button uk-margin-auto uk-button-primary" value="Send me link" />
        </center>
      </div>
    </div>
  </div>
</form>
@endsection
