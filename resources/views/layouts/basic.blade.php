<!DOCTYPE html>
<html class="uk-background-muted">
<head>
  @include('layouts.header')
</head>
<body>
  @include('layouts.nav')
  @yield('content')
</body>
  @include('layouts.footer')
</html>