@if(Auth::check())
<nav class="uk-navbar-container" uk-navbar>
  <div class="uk-navbar-left"><span class="uk-navbar-item uk-text-lead"><span uk-icon="icon: bolt; ratio: 1.5"></span>TARS</span></div>
  <div class="uk-navbar-right">
  @if(Request::url() !== route('student.index.get'))
    <a href="{{route('student.index.get')}}"><span class="uk-navbar-item uk-text-small"><span uk-icon="icon: users; ratio: 1"></span>&nbsp;&nbsp;All Students</span></a>
  @endif
  <a href="javascript:void(0)" onclick="$('#logoutForm').submit()"><span class="uk-navbar-item uk-text-small"><span uk-icon="icon: sign-out; ratio: 1"></span>Logout</span></a>
  </div>
</nav>
<form id="logoutForm" action="{{route('login.logout.post')}}" method="POST">
  {{csrf_field()}} 
</form>
@endif
