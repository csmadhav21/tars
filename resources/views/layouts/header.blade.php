<title>TARS: @yield('title')</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.19/css/uikit.min.css" />
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
  html,body {
    height: 100%;    
  }
  * {
    font-family: "Roboto"!important;
  }
  .uk-navbar-container:not(.uk-navbar-transparent) {
      background: #1e87f0;
  }
  .uk-navbar *, .uk-navbar-nav>li>a {
    color: #d8f4ff;
  }
</style>