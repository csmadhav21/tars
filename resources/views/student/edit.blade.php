@extends('layouts.basic')

@section('title','Create Student')

@section('content')
<form action="{{route('student.store.put',['id'=>$student['id']])}}"  class="uk-form-horizontal" method="POST">
    <div class="uk-margin-large-top">
    <div class="uk-card uk-card-default uk-width-1-2@m uk-margin-auto uk-margin-auto-vertical" style="width: 800px;">
        <div class="uk-card-header">
            <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-expand">
                    <h3 class="uk-card-title uk-margin-remove-bottom">Edit Student</h3>
                </div>
            </div>
        </div>
        <div class="uk-card-body">
            @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                      <div class="uk-alert-danger" uk-alert>
                          <a class="uk-alert-close" uk-close></a>
                          <p>{{$error}}</p>
                      </div>
                    @endforeach
            @endif
            <div class="uk-margin">
                <label class="uk-form-label">First Name:</label>
                <div class="uk-form-controls">
                    <input class="uk-form-width-large uk-input" value="{{$student->first_name}}" name="first_name">
                </div>
            </div>

            <div class="uk-margin">
                <label class="uk-form-label">Last Name: </label>
                <div class="uk-form-controls">
                    <input class="uk-form-width-large uk-input" value="{{ !is_null(old('last_name')) ? old('last_name') : $student->last_name}}" name="last_name">
                </div>
            </div>
            <div class="uk-margin">
                <label class="uk-form-label">Address: </label>
                <div class="uk-form-controls">
                    <textarea class="uk-form-width-large uk-input" name="address">{{$student->address}}</textarea>
                </div>
            </div>
            <div class="uk-margin">
                <label class="uk-form-label">Gender: </label>
                <div class="uk-form-controls">
                    <label>
                        <input class="uk-radio" type="radio" name="gender" value="1" id="r1" {{ ($student->gender==1)? "checked" :""  }}> Male 
                    </label>
                    <label for="r2">
                        <input class="uk-radio" type="radio" name="gender" value="2" id="r2" {{ ($student->gender==2)? "checked" :""  }}> Female
                    </label>
                </div>
            </div>
            <div class="uk-margin">
                <label class="uk-form-label">Year of passing: </label>
                <div class="uk-form-controls">
                  <select name="year_of_passing" class="uk-select">
                    @for($i = config('core.year_of_passing')["start"]; $i < config('core.year_of_passing')["end"]; ++$i)
                        <option value="{{$i}}" @if($i == $student->year_of_passing) selected @endif >{{$i}}</option>
                    @endfor
                  </select>
                </div>
            </div>
            <div class="uk-margin">
                <label class="uk-form-label">Interests: </label>
                <div class="uk-form-controls">
                    @foreach($interests as $interest)
                        <label>
                            <input type="checkbox" class="uk-checkbox" name="interests[]" value="{{$interest->id}}" {{ $student->interests->where('id',$interest->id)->first() ? 'checked' : '' }}>&nbsp;{{$interest->name}}&nbsp;
                        </label>
                    @endforeach
                </div>
            </div>
            <input type="hidden" name="id" value="{{$student->id}}">
            <input type="hidden" name="_method" value="PUT">
            {{csrf_field()}}
        </div>
        <div class="uk-card-footer">
            <a href="#" class="uk-button uk-button-text "><input type="submit" class="uk-button uk-margin-auto uk-button-primary" value="Save" /></a>
            
        </div>
    </div>
    </div>
</form>
@endsection