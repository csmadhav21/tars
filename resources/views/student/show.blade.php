@extends('layouts.basic')

@section('title','dashboard')

@section('content')
@include('flash::message')
<div class="uk-margin-right uk-margin-top uk-text-right">
    <span uk-icon="icon: plus"></span>&nbsp;<a href="{{route('student.create.get')}}">Add a student</a>
</div>
<div class="uk-card uk-card-body uk-margin-auto uk-overflow-auto">
    <table class="uk-table uk-table-middle uk-table-hover uk-table-striped uk-table-small">
        <caption>All Students</caption>
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Gender</th>
                <th>Year of passing</th>
                <th>Interests</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
             @foreach ($students as $student)
                <tr>
                    <td>{{$student->first_name}}</td>
                    <td>{{$student->last_name}}</td>
                    <td>{{$student->address}}</td>
                    <td>{{array_flip(config('core.gender'))[$student->gender]}}</td>
                    <td>{{$student->year_of_passing}}</td>
                    <td>
                        @if(count($student->interests)>0)
                            @foreach($student->interests as $interest)
                                <span class="uk-badge">{{$interest->name}}</span>
                            @endforeach
                        @else
                            No interests
                        @endif
                    </td>
                    <td><a href="{{route('student.edit.get',['id'=>$student->id])}}"><span uk-icon="icon: file-edit"></span>&nbsp;Edit</a>
                        <form action="{{route('student.delete.delete',['id',$student->id])}}" method="POST" id="del_{{$student->id}}" onsubmit="return confirm('Are you sure you want to delete this student?')">
                            <input type="hidden" name="id" value="{{$student->id}}">
                            {{ method_field('DELETE') }}
                            {{csrf_field()}}
                        </form>
                        <a href="javascript:void(0)" onclick="$('#del_{{$student->id}}').submit()"><span uk-icon="icon: trash"></span>&nbsp;Delete</a>
                    </td>
                </tr>
            @endforeach
            
        </tbody>
    </table>
    <div class="uk-margin-auto">{{($students)->links()}}</div>
</div>

@endsection


