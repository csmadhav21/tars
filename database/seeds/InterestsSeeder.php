<?php

use App\Interest;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class InterestsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Interest::truncate();

    $interests = [
      ['name' => 'dancing'],
      ['name' => 'chess'],
      ['name' => 'cricket'],
      ['name' => 'swimming'],
    ];
    
    $now = Carbon::now()->toDateTimeString();

    foreach($interests as &$interest) {
       $interest['updated_at'] = $interest['created_at'] = $now;
    }

    Interest::insert($interests);
  }
}
