<?php

return [

  'gender' => [

    'male' => 1,
    'female' => 2,
  
  ],
  'year_of_passing' => [
    'start' => 2010,
    'end' => 2019
  ]
];
