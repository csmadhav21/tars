<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@index')->name('login.index.get');
Route::post('/login','Auth\LoginController@login')->name('login.login.post');

Route::get('/register', 'Auth\RegisterController@index')->name('register.index.get');
Route::post('/register','Auth\RegisterController@register')->name('register.register.post');

Route::get('/password/forgot','Auth\ForgotPasswordController@index')->name('forgotPassword.index.get');
Route::post('/password/forgot','Auth\ForgotPasswordController@sendMailAfterAuth')->name('forgotPassword.sendMailAfterAuth.post');

Route::get('/password/reset','Auth\ResetPasswordController@index')->name('resetPassword.index.get');
Route::post('/password/reset','Auth\ResetPasswordController@reset')->name('resetPassword.reset.post');

Route::group(['middleware' => 'auth'], function() {

  Route::post('/logout', 'Auth\LoginController@logout')->name('login.logout.post');

  Route::group(['prefix' => 'students'], function() {
    Route::get('/','StudentController@index')->name('student.index.get');

    Route::get('/create','StudentController@create')->name('student.create.get');

    Route::get('/{id}/edit', 'StudentController@edit')
          ->where('id', '[0-9]+')
          ->name('student.edit.get');

    Route::put('/{id}','StudentController@update')
         ->where('id', '[0-9]+')
         ->name('student.store.put');
    
    Route::post('/','StudentController@store')->name('student.store.post');
    Route::delete('/{id}','StudentController@delete')->name('student.delete.delete');

  });
});
