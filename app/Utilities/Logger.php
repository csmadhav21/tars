<?php

namespace App\Utilities;

use Monolog\Logger as Monologger;
use Monolog\Handler\StreamHandler;

class Logger
{
  /**
   * Log something to a file
   *
   * @param  string $key
   * @param  string $file
   * @param  string $message
   *
   * @return null
   */
  public function file($key, $file, $message)
  {
    $logger = new Monologger($key);

    $logger->pushHandler(new StreamHandler(storage_path() . '/logs/'. $file , Monologger::INFO));

    $logger->addInfo($message);
  }
}