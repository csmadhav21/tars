<?php

namespace App\Utilities;

use Carbon\Carbon;
use Mail;

class Mailer
{
  /**
   * Constructor
   */
  public function __construct()
  {
    $this->logger = new Logger;
  }

  /**
   * Send Mail In Queue
   *
   * @param  string $view
   * @param  string $email
   * @param  string $subject
   * @param  array  $data
   * @param  array  $bcc
   * @param  array $attachments
   *
   * @return null
   */
  public function queued($view, $email, $subject, array $data, $bcc = [], $attachments = [])
  {
    Mail::queue($view, $data, function ($message) use ($email, $subject, $bcc, $attachments) {
      $message->to($email);
      $message->subject($subject);

      if(count($bcc)) {
        $message->bcc($bcc);
      }

      if(count($attachments)) {
        $message->bcc($attachments);
      }
    });

    $this->logMail($email, $subject, $data, $bcc, $attachments);
  }

  /**
   * Send Mail Normally
   *
   * @param  string $view
   * @param  string $email
   * @param  string $subject
   * @param  array  $data
   * @param  array  $bcc
   * @param  array $attachments
   *
   * @return null
   */
  public function normal($view, $email, $subject, array $data, $bcc = [], $attachments = [])
  {
    Mail::send($view, $data, function ($message) use ($email, $subject, $bcc, $attachments) {
      $message->to($email);
      $message->subject($subject);

      if(count($bcc)) {
        $message->bcc($bcc);
      }

      if(count($attachments)) {
        $message->bcc($attachments);
      }
    });

    $this->logMail($email, $subject, $data, $bcc, $attachments);
  }

  /**
   * Log the sent mail
   *
   * @param  string $email
   * @param  string $subject
   * @param  array  $data
   * @param  array  $bcc
   * @param  array $attachments
   *
   * @return null
   */
  protected function logMail($email, $subject, array $data, $bcc = [], $attachments = [])
  {
    $this->logger->file('mails', 'mail.log', '--------------- Mail Log ---------------');
    $this->logger->file('mails', 'mail.log', 'Time : ' . Carbon::now()->format('j M, Y (h:i:s A)'));
    $this->logger->file('mails', 'mail.log', 'Email : ' . $email);
    $this->logger->file('mails', 'mail.log', 'Subject : ' . $subject);
    $this->logger->file('mails', 'mail.log', 'Data : ' . json_encode($data));

    if(count($bcc)) {
      $this->logger->file('mails', 'mail.log', 'BCC : ' . json_encode($bcc));
    }

    if(count($attachments)) {
      $this->logger->file('mails', 'mail.log', 'Attachments : ' . json_encode($attachments));
    }
  }
}