<?php

namespace App\Repositories;

use DB;

class ForgotPasswordRepository {
	
	public function saveToken($email, $token) {
    return DB::table('password_resets')->insert([
      'email' => $email,
      'token' => $token,
      'created_at' => date('Y-m-d H:i:s')
    ]);
  }
}