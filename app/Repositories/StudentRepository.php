<?php
namespace App\Repositories;

use App\Student;
use App\Traits\DbRepositoryTrait;

class StudentRepository {
  
  use DbRepositoryTrait;

  protected $model = Student::class;

  public function create($inputs) {
    $student = Student::create($inputs);

    $this->syncInterests($student, $inputs);
  }

  public function get($id) {
    return $this->getWhere('id',$id)->first();
  }

  public function updateStudent($student, $attributes) {
    $student->update($attributes);

    $this->syncInterests($student, $attributes);
  }

  public function syncInterests($student, $inputs) {
    if(isset($inputs['interests']) && count($inputs['interests'])) {      
      $student->interests()->sync($inputs['interests']);
    } else {
      $student->interests()->sync([]);
    }
  }
}