<?php
namespace App\Repositories;

use App\Interest;
use App\Traits\DbRepositoryTrait;

class InterestsRepository {

  use DbRepositoryTrait;

  protected $model = Interest::class;
  
}