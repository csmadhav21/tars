<?php 
namespace App\Repositories;

use DB;
use App\User;
use App\Traits\DbRepositoryTrait;

class UserRepository {

  use DbRepositoryTrait;

  protected $model = User::class;

  public function deleteAllTokens($inputs) {
    DB::table('password_resets')->where('email', $inputs['email'])->delete();
  }
}