<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'gender','address', 'year_of_passing'
    ];

    public function interests() {
      return $this->belongsToMany('App\Interest');
    }
}
