<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
	/**
	 * Perform post-registration booting of services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Response::macro('success', function ($data) {
			return Response::json([
				'data' => $data,
			]);
		});

		Response::macro('error', function ($errors, $status = 400) {
			$errors = is_array($errors) ? $errors : ['message' => $errors];

			return Response::json([
				'errors' => $errors,
			], $status);
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
