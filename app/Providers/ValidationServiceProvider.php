<?php

namespace App\Providers;

use DB;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidationServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    Validator::extend('password_token_expiry', function ($attribute, $value, $parameters, $validator) {
      $row = DB::table('password_resets')->where('token', $value)->first();

      if(! $row) {
        return false;
      }

      $createdAt = Carbon::parse($row->created_at)->addHours(1);

      return $createdAt->isPast() ? false : true;
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
      //  
  }
}
