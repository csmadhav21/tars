<?php
namespace App\Services;

use App\Validators\StudentValidator;
use App\Repositories\StudentRepository;
use App\Repositories\InterestsRepository;

class StudentService {

  public function __construct(StudentRepository $studentRepo, StudentValidator $studentValidator, InterestsRepository $interestsRepo) {
    $this->studentRepo = $studentRepo;
    $this->interestsRepo = $interestsRepo;
    $this->studentValidator = $studentValidator;
  }

  public function getAllInterests() {
    return $this->interestsRepo->all();
  }

  public function paginate() {
    return $this->studentRepo->paginate(10);
  }

  public function get($id) {
    return $this->studentRepo->get($id);
  }

  public function create($inputs) {
    $this->studentValidator->fire($inputs);

    $this->studentRepo->create($inputs);
  }

  public function update($student, $inputs) {
    
    $this->studentValidator->fire($inputs,'edit');
    
    $this->studentRepo->updateStudent($student, array_except($inputs, ['_method','_token']));
  }

  public function delete($student) {
    $this->studentRepo->delete($student);
  }
}