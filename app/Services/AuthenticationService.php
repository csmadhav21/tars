<?php
namespace App\Services;

use Hash;
use App\Validators\LoginValidator;
use App\Repositories\UserRepository;
use App\Validators\RegisterValidator;
use App\Validators\ForgotPasswordValidator;
use App\Validators\ResetPasswordValidator;

class AuthenticationService {

  public function __construct(UserRepository $userRepo) {
    $this->userRepo = $userRepo;
  }

  public function register(RegisterValidator $validation, $inputs) {
    $validation->fire($inputs);

    $inputs['password'] = Hash::make($inputs['password']);
    
    return $this->userRepo->create($inputs);
  }

  public function login(LoginValidator $loginValidation, $inputs) {
    $loginValidation->fire($inputs);
  }

  public function forgotPassword(ForgotPasswordValidator $forgotPasswordValidator, $inputs) {
    $forgotPasswordValidator->fire($inputs);
  }

  public function resetPasswordVerify(ResetPasswordValidator $resetPasswordValidator,$inputs) {
    $resetPasswordValidator->fire($inputs, 'Verify');
  }

  public function resetPassword(ResetPasswordValidator $resetPasswordValidator, $inputs) {
    $resetPasswordValidator->fire($inputs,'Verify');  // Verifying wether email token is valid and then check the password 
    
    $resetPasswordValidator->fire($inputs);

    $this->userRepo->deleteAllTokens($inputs);
    
    return $this->userRepo->getWhere('email', $inputs['email'])->first()->update(['password'=>Hash::make($inputs["password"])]);
  }
}