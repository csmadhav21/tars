<?php

namespace App\Services;

use Mail;
use App\Mail\ForgotPasswordMail;
use App\Repositories\ForgotPasswordRepository;

class ForgotPasswordService {
	
  public function __construct(ForgotPasswordRepository $forgotPasswordRepository) {
    
    $this->email = request()->get('email');

    $this->forgotPasswordRepository = $forgotPasswordRepository;
  }

  public function generateToken() {
    $this->token = sha1(time());
  }

  public function generateLink() {
    return route('resetPassword.index.get', [
      'email' => $this->email,
      'token' => $this->token,
    ]);
  }

  public function sendMail() {
    $mail = new ForgotPasswordMail([
      'link' => $this->generateLink(), 'email' => $this->email
    ]);

    Mail::to($this->email)->queue($mail);
  }

  public function handleRequest() {
    $this->generateToken();

    $this->forgotPasswordRepository->saveToken($this->email, $this->token);
    
    $this->sendMail();
  }
}