<?php

namespace App\Http\Controllers;

use App\Services\StudentService;

class StudentController extends Controller
{
  protected $studentService;

  public function __construct(StudentService $studentService) {
    $this->studentService = $studentService;
  }

  public function index() {
    $students = $this->studentService->paginate();

    return view('student.show', compact('students'));
  }

  public function store() {
    $inputs = request()->all();

    $this->studentService->create($inputs);

    flash()->success('Student created successfully');

    return redirect(route('student.index.get'));
  }

  public function update($id) {
    $inputs = request()->all();

    $student = $this->studentService->get($id);

    $this->studentService->update($student, $inputs);

    flash()->success('Student edited successfully');

    return redirect(route('student.index.get'));
  }

  public function edit($id) {
    $student = $this->studentService->get($id);
    
    $interests = $this->studentService->getAllInterests();

    return view("student.edit", compact(['interests', 'student']));
  }

  public function create() {
    $interests = $this->studentService->getAllInterests();

    return view("student.create", compact('interests'));
  }

  public function delete() {
    $student = $this->studentService->get(request()->get('id'));

    $this->studentService->delete($student);

    flash()->success('Student Deleted successfully');

    return redirect(route('student.index.get'));
  }
}
