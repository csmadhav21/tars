<?php

namespace App\Http\Controllers\Auth;

use App\Services\AuthenticationService;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function index() {
    return view('login');
  }

  public function __construct(AuthenticationService $authService) {
    $this->redirectTo = route('student.index.get');

    $this->middleware('guest', ['except' => 'logout']);
    
    $this->authService = $authService;
  }

  public function login() {
    $inputs = request()->only(['email','password']);
    
    app()->call([$this->authService, 'login'], ['inputs' => $inputs]);
    
    if(auth()->attempt($inputs)) {
      return redirect(route('student.index.get'));
    } else {
      flash()->danger('Invalid credentials.');

      return redirect()->back()->withInput();
    }
  }
  
  public function logout() {
    auth()->logout();

    return redirect('/');
  }
}
