<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\AuthenticationService;

class RegisterController extends Controller
{
  public function __construct(AuthenticationService $authService) {
    $this->middleware('guest');
    
    $this->authService = $authService;
  }

  public function login() {
    $inputs = request()->only(['email', 'password']);

    if(auth()->attempt($inputs)) {
      return redirect();
    } else {
      return redirect()->back();
    }
  }

  public function register() {
    $user = app()->call([$this->authService, 'register'], ['inputs' => request()->all()]);

    auth()->login($user);
    
    return redirect(route('student.index.get'));
  }

  public function index() {
    return view('register');
  }
}
