<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\AuthenticationService;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Validation\ValidationException;

class ResetPasswordController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Password Reset Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for handling password reset requests
  | and uses a simple trait to include this behavior. You're free to
  | explore this trait and override any methods you wish to tweak.
  |
  */

  use ResetsPasswords;

  /**
   * Where to redirect users after resetting their password.
   *
   * @var string
   */
  protected $redirectTo;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(AuthenticationService $authenticationService)
  {
    $this->redirectTo = route('login.index.get');

    $this->middleware('guest');

    $this->authenticationService = $authenticationService;
  }

  public function index() {
    try {
      app()->call([$this->authenticationService, 'resetPasswordVerify'], ['inputs' => request()->all()]);
    } catch(ValidationException $e) {
      flash()->error('Authentication failed please try again.');

      return redirect(route('forgotPassword.index.get'));
    }

    return view("auth.passwords.ResetPassword");
  }

  public function reset() {
    app()->call([$this->authenticationService, 'resetPassword'], ['inputs'=>request()->all()]);

    return redirect(route('login.index.get'));
  }
}
