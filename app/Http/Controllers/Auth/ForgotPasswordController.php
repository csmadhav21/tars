<?php

namespace App\Http\Controllers\Auth;

use App\Traits\ForgotPasswordTrait;
use App\Http\Controllers\Controller;
use App\Services\AuthenticationService;
use App\Services\ForgotPasswordService;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Password Reset Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for handling password reset emails and
  | includes a trait which assists in sending these notifications from
  | your application to your users. Feel free to explore this trait.
  |
  */
  protected $authService;

  use ForgotPasswordTrait;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(AuthenticationService $authService, ForgotPasswordService $forgotPasswordService)
  {
    $this->authService = $authService;
    $this->forgotPasswordService = $forgotPasswordService;
    $this->middleware('guest');
  }
  public function index() {
    return $this->showLinkRequestForm();
  }

  public function sendMailAfterAuth() {
    app()->call([$this->authService,'forgotPassword'],['inputs'=>request()->all()]);

    $this->forgotPasswordService->handleRequest();

    flash()->success('A mail with reset link has been sent to your email address.');

    return redirect(route('login.index.get'));
  }
}
