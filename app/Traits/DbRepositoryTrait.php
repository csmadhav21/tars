<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;

trait DbRepositoryTrait
{
	/**
	 * Get all of the models from the database.
	 *
	 * @param array $related
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function all(array $related = null)
	{
		$modelName = $this->model;

		return $modelName::all();
	}

	/**
	 * Get the paginated models from the database.
	 *
	 * @param  int $perPage
	 * @return \Illuminate\Pagination\LengthAwarePaginator
	 */
	public function paginate($perPage = 20)
	{
		$modelName = $this->model;

		return $modelName::orderBy('id', 'desc')->paginate($perPage);
	}

	/**
	 * Get a model by its primary key.
	 *
	 * @param int $id
	 * @param array $related
	 * @return \Illuminate\Database\Eloquent\Model
	 *
	 * @throws \App\Exceptions\ModelNotFoundException
	 */
	public function get($id, array $related = null)
	{
		$modelName = $this->model;

		$models = $modelName::find($id);

		if(!$models) throw new ModelNotFoundException('No ' . str_replace('_', ' ', snake_case(class_basename($modelName))) . ' record found.');

		return $models;
	}

	/**
	 * Get models by the value.
	 *
	 * @param int $id
	 * @param array $related
	 * @return \Illuminate\Database\Eloquent\Collection
	 *
	 * @throws \App\Exceptions\ModelNotFoundException
	 */
	public function getWhere($column, $value, array $related = null)
	{
		$modelName = $this->model;

		$models = $modelName::where($column, $value);

		if(!is_null($related)) {
			foreach($related as $key => $value) {
				$models = $models->where($key, $value);
			}
		}

		$models = $models->get();

		if($models->isEmpty()) throw new ModelNotFoundException('No ' . str_replace('_', ' ', snake_case(class_basename($modelName))) . ' record found.');

		return $models;
	}

	/**
	 * Get the model data by adding the given query
	 *
	 * @param  string     $column
	 * @param  mixed      $value
	 * @param  array|null $related
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getWhereIn($column, $value, array $related = null)
	{
		$modelName = $this->model;

		$models = $modelName::whereIn($column, $value);

		if(!is_null($related)) {
			foreach($related as $key => $value) {
				$models = $models->where($key, $value);
			}
		}

		$models = $models->get();

		if($models->isEmpty()) throw new ModelNotFoundException('No ' . str_replace('_', ' ', snake_case(class_basename($modelName))) . ' record found.');

		return $models;
	}

	/**
	 * Get models by the value with relations.
	 *
	 * @param int $id
	 * @param array $related
	 * @return \Illuminate\Database\Eloquent\Collection
	 *
	 * @throws \App\Exceptions\ModelNotFoundException
	 */
	public function getWith($id, array $related = null)
	{
		$modelName = $this->model;

		if(!is_null($related)) {
			$models = $modelName::with($related)->find($id);
		} else {
			$models = $modelName::find($id);
		}

		if(!$models) throw new ModelNotFoundException('No ' . str_replace('_', ' ', snake_case(class_basename($modelName))) . ' record found.');

		return $models;
	}

	/**
	 * Get models as goup.
	 *
	 * @param int $id
	 * @param array $related
	 * @return \Illuminate\Database\Eloquent\Collection
	 *
	 * @throws \App\Exceptions\ModelNotFoundException
	 */
	public function getGroup($name)
	{
		$modelName = $this->model;

		$models = $modelName::groupBy($name)->get();

		return $models;
	}

	/**
	 * Save a new model and return the instance.
	 *
	 * @param array $attributes
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public function create(array $attributes)
	{
		$modelName = $this->model;

		return $modelName::create($attributes);
	}
	/**
	 * Get the first record matching the attributes or instantiate it.
	 *
	 * @param array $attributes
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public function firstOrNew(array $attributes)
	{
		$modelName = $this->model;

		return $modelName::firstOrNew($attributes);
	}

	/**
	 * Get the first record matching the attributes or create it.
	 *
	 * @param array $attributes
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public function firstOrCreate(array $attributes)
	{
		$modelName = $this->model;

		return $modelName::firstOrCreate($attributes);
	}

	/**
	 * Update the model by the given attributes.
	 *
	 * @param  \App\Models\Model $model
	 * @return bool|int
	 */
	public function update($model, $attributes)
	{
		return $model->update($attributes);
	}

	/**
	 * Delete the model from the database.
	 *
	 * @param  int|\App\Models\Model $model
	 * @return bool|null
	 *
	 * @throws \Exception
	 */
	public function delete($model)
	{
		if(is_numeric($model)) {
			return $this->get($model)->delete();
		} else {
			return $model->delete();
		}
	}

	/**
	 * Get all entries between 2 dates
	 *
	 * @param  string $start
	 * @param  string $end
	 * @param  array  $related
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function allBetweenDates($start, $end, array $related = null)
  {
		$modelName = $this->model;

		$modelName = $modelName::where('created_at', '>=', $start)->where('created_at', '<=', $end);

		if($related) {
			foreach($related as $relation) {
				$modelName = $modelName->with($relation);
			}
		}

    return $modelName->get();
  }

	/**
	 * Get all entries between 2 dates by type
	 *
	 * @param  string $start
	 * @param  string $end
	 * @param  array  $type
	 * @param  array  $related
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function allBetweenDatesByType($start, $end, $type, array $related = null)
  {
		$modelName = $this->model;

		$modelName = $modelName::where('created_at', '>=', $start)->where('created_at', '<=', $end)->where($type['key'], $type['value']);

		if($related) {
			foreach($related as $relation) {
				$modelName = $modelName->with($relation);
			}
		}

		return $modelName->get();
  }
}
