<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait ForgotPasswordTrait {

  public function sendResetLinkResponse(Request $request) {
    $email = $request->get('email');
    $link  = url('/password/reset')."?email=".$email."&t=".sha1(time());
    Mail::to($email)->send(new ForgotPasswordMail($link));

  }
  public function showLinkRequestForm() {

    return view('auth.passwords.ForgotPassword');
  
  }
}