<?php
namespace App\Validators;

class StudentValidator extends Validator
{
  /*
  * Rules to get the client onboard
  */
  public function rules($type = null, $inputs = [], $data = []) {
    $basicValidations = [
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'address' => 'required|max:255',
      'gender' => 'required|in:' . implode(',', config('core.gender')),
      'year_of_passing' => 'required|numeric|date_format:Y',
      'interests.*' => 'exists:interests,id',
    ];
    if($type) {
      $basicValidations['id'] = 'required|numeric';
    }
    return $basicValidations;
  }
    

}