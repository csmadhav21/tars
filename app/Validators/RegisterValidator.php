<?php
namespace App\Validators;

class RegisterValidator extends Validator
{
  /*
  * Rules to get the client onboard
  */
  public function rules($type = '', $inputs = [], $data = []) {
    return [
      'email' => 'required|email|unique:users',
      'password' => 'required|confirmed|min:6'
    ];
  }

}