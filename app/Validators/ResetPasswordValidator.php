<?php

namespace App\Validators;

class ResetPasswordValidator extends Validator {
  
	public function rules($type = null, $inputs = [], $data = []) {
    if($type == 'Verify') {
      return [
        'email' => 'required|exists:password_resets',
        'token' => 'required|exists:password_resets,token|password_token_expiry'
      ];
    } else {
      return [
        'password' => 'required|confirmed'
      ];
    }
	}

  public function messages() {
    return [
      'token.password_token_expiry' => 'This Token has been expired.'
    ];
  }
}