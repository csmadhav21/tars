<?php
namespace App\Validators;

class LoginValidator extends Validator
{
  /*
  * Rules to get the client to dashboard
  */
  public function rules($type = '', $inputs = [], $data = []) {
    return [
      'email' => 'required|exists:users',
      'password' => 'required|min:6'
    ];
  }

}