<?php

namespace App\Validators;

class ForgotPasswordValidator extends Validator {

  /*
  * Rules to check whether the request has valid email 
  */
  public function rules($type = null, $inputs = [], $data = []) {
    return [
      'email'=>'required|email|exists:users'
    ];
  }
}