<?php

namespace App\Validators;

use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

abstract class Validator
{
  /**
   * \Illuminate\Validation\Factory
   *
   * @var string
   */
  protected $validator;

  /**
   * Type of rule
   *
   * @var string
   */
  protected $type = '';

  /**
   * Constructor
   *
   * @param \Illuminate\Validation\Validator
   */
  public function __construct(Factory $validator)
  {
    $this->validator = $validator;
  }

  /**
   * Rules
   *
   * @param  string $type
   * @param  array  $inputs
   * @param  array  $data
   *
   * @return array
   */
  protected function rules($type = '', $inputs = [], $data = [])
  {
    return [
      //
    ];
  }

  /**
   * Messages
   *
   * @return array
   */
  public function messages()
  {
    return [
      'resolution' => 'The resolution is not correct',
    ];
  }

  /**
   * Fire
   *
   * @param  array  $inputs
   * @param  string $type
   * @param  array  $data
   *
   * @return bool
   */
  public function fire($inputs, $type = '', array $data = [])
  {
    $validation = $this->validator->make($inputs, $this->rules($type, $inputs, $data), $this->messages());

    if($validation->fails()) {
      throw new ValidationException($validation, $inputs);
    }

    return true;
  }
}
